{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE BlockArguments #-}

module Internal
  ( Parser
  , module Text.Megaparsec
  , module Text.Megaparsec.Char
  , m4_version_t
  , bash_man_file
  , bs_datetime
  , majorP
  , minorP
  , patchP
  , pDt
  , p_batt_stat_record
  , p_batt_stat_record2
  , p_batt_stat_record3
  , batt_stat_test_data1
  , batt_stat_test_data2
  , batt_stat_test_data3
  , batt_stat_test_data4
  , batt_stat_test_data5
  , defineP
  , p_ver_var_name
  , VersionNumber(..)
  , VersionDetails(..)
  , VersionNumberName(..)
  , buildVersion
  , nullVersionDetails
  , BattStatRecord(..)
  , nullBattStatRecord
  , convert_to_csv
  --, convert_unix_date_to_txt
  --, eolof
  ) where

import Data.Text (Text)
import qualified Data.Text as T
import Text.Megaparsec
import Text.Megaparsec.Char

import Data.Version
import Data.Void
import qualified Text.Megaparsec.Char.Lexer as L

import System.Process (readProcess)

--import qualified Data.Set as Set
import Text.Megaparsec.Debug
import Text.Printf

import Data.List (intercalate, intersperse)

import Control.Monad(void)

-- | We add the Parser type synonym that we will use to resolve ambiguity in 
-- the type of the parsers:
type Parser = Parsec Void Text

--type TextParser m a = ParsecT CustomErr Text m a

m4_version_t :: T.Text
m4_version_t = T.pack "define([VERSION_NUMBER],[0.1.1])dnl"

bash_man_file :: T.Text
bash_man_file = ".SH NAME\nbash \\- GNU Bourne-Again SHell"

digitsP :: Parsec Void T.Text Int
digitsP = read <$> ((T.unpack <$> string "0") <|> some digitChar)

majorP :: Parsec Void T.Text Int
majorP = digitsP -- <* char '.'

minorP :: Parsec Void T.Text Int
minorP = digitsP

patchP :: Parsec Void T.Text Int
patchP = digitsP

-- | batt-stat data
batt_stat_test_data1 :: T.Text
batt_stat_test_data1 =
  "1609862298|POWER_SUPPLY_NAME=BAT0\nPOWER_SUPPLY_TYPE=Battery\nPOWER_SUPPLY_STATUS=Discharging\nPOWER_SUPPLY_PRESENT=1\nPOWER_SUPPLY_TECHNOLOGY=Li-poly\nPOWER_SUPPLY_CYCLE_COUNT=490\nPOWER_SUPPLY_VOLTAGE_MIN_DESIGN=11580000\nPOWER_SUPPLY_VOLTAGE_NOW=11290000\nPOWER_SUPPLY_POWER_NOW=8546000\nPOWER_SUPPLY_ENERGY_FULL_DESIGN=57000000\nPOWER_SUPPLY_ENERGY_FULL=59230000\nPOWER_SUPPLY_ENERGY_NOW=26210000\nPOWER_SUPPLY_CAPACITY=44\nPOWER_SUPPLY_CAPACITY_LEVEL=Normal\nPOWER_SUPPLY_MODEL_NAME=02DL013\nPOWER_SUPPLY_MANUFACTURER=LGC\nPOWER_SUPPLY_SERIAL_NUMBER= 6349\n\n"

batt_stat_test_data2 :: T.Text
batt_stat_test_data2 =
  "1614061363|POWER_SUPPLY_NAME=BAT0\nPOWER_SUPPLY_TYPE=Battery\nPOWER_SUPPLY_STATUS=Discharging\nPOWER_SUPPLY_PRESENT=1\nPOWER_SUPPLY_TECHNOLOGY=Li-poly\nPOWER_SUPPLY_CYCLE_COUNT=580\nPOWER_SUPPLY_VOLTAGE_MIN_DESIGN=11580000\nPOWER_SUPPLY_VOLTAGE_NOW=12017000\nPOWER_SUPPLY_POWER_NOW=9048000\nPOWER_SUPPLY_ENERGY_FULL_DESIGN=57000000\nPOWER_SUPPLY_ENERGY_FULL=59230000\nPOWER_SUPPLY_ENERGY_NOW=52850000\nPOWER_SUPPLY_CAPACITY=89\nPOWER_SUPPLY_CAPACITY_LEVEL=Normal\nPOWER_SUPPLY_MODEL_NAME=02DL013\nPOWER_SUPPLY_MANUFACTURER=LGC\nPOWER_SUPPLY_SERIAL_NUMBER= 6349"
batt_stat_test_data3 :: T.Text
batt_stat_test_data3 =
  "1614153032,POWER_SUPPLY_NAME=BAT0,POWER_SUPPLY_TYPE=Battery,POWER_SUPPLY_STATUS=Discharging,POWER_SUPPLY_PRESENT=1,POWER_SUPPLY_TECHNOLOGY=Li-poly,POWER_SUPPLY_CYCLE_COUNT=582,POWER_SUPPLY_VOLTAGE_MIN_DESIGN=11580000,POWER_SUPPLY_VOLTAGE_NOW=11748000,POWER_SUPPLY_POWER_NOW=9551000,POWER_SUPPLY_ENERGY_FULL_DESIGN=57000000,POWER_SUPPLY_ENERGY_FULL=59230000,POWER_SUPPLY_ENERGY_NOW=46000000,POWER_SUPPLY_CAPACITY=77,POWER_SUPPLY_CAPACITY_LEVEL=Normal,POWER_SUPPLY_MODEL_NAME=02DL013,POWER_SUPPLY_MANUFACTURER=LGC,POWER_SUPPLY_SERIAL_NUMBER= 6349\n1614153149,POWER_SUPPLY_NAME=BAT0,POWER_SUPPLY_TYPE=Battery,POWER_SUPPLY_STATUS=Discharging,POWER_SUPPLY_PRESENT=1,POWER_SUPPLY_TECHNOLOGY=Li-poly,POWER_SUPPLY_CYCLE_COUNT=582,POWER_SUPPLY_VOLTAGE_MIN_DESIGN=11580000,POWER_SUPPLY_VOLTAGE_NOW=11824000,POWER_SUPPLY_POWER_NOW=9553000,POWER_SUPPLY_ENERGY_FULL_DESIGN=57000000,POWER_SUPPLY_ENERGY_FULL=59230000,POWER_SUPPLY_ENERGY_NOW=45510000,POWER_SUPPLY_CAPACITY=76,POWER_SUPPLY_CAPACITY_LEVEL=Normal,POWER_SUPPLY_MODEL_NAME=02DL013,POWER_SUPPLY_MANUFACTURER=LGC,POWER_SUPPLY_SERIAL_NUMBER= 6349\n"
{-
1614153269,POWER_SUPPLY_NAME=BAT0,POWER_SUPPLY_TYPE=Battery,POWER_SUPPLY_STATUS=Discharging,POWER_SUPPLY_PRESENT=1,POWER_SUPPLY_TECHNOLOGY=Li-poly,POWER_SUPPLY_CYCLE_COUNT=582,POWER_SUPPLY_VOLTAGE_MIN_DESIGN=11580000,POWER_SUPPLY_VOLTAGE_NOW=11821000,POWER_SUPPLY_POWER_NOW=10118000,POWER_SUPPLY_ENERGY_FULL_DESIGN=57000000,POWER_SUPPLY_ENERGY_FULL=59230000,POWER_SUPPLY_ENERGY_NOW=44990000,POWER_SUPPLY_CAPACITY=75,POWER_SUPPLY_CAPACITY_LEVEL=Normal,POWER_SUPPLY_MODEL_NAME=02DL013,POWER_SUPPLY_MANUFACTURER=LGC,POWER_SUPPLY_SERIAL_NUMBER= 6349
1614153332,POWER_SUPPLY_NAME=BAT0,POWER_SUPPLY_TYPE=Battery,POWER_SUPPLY_STATUS=Discharging,POWER_SUPPLY_PRESENT=1,POWER_SUPPLY_TECHNOLOGY=Li-poly,POWER_SUPPLY_CYCLE_COUNT=582,POWER_SUPPLY_VOLTAGE_MIN_DESIGN=11580000,POWER_SUPPLY_VOLTAGE_NOW=11834000,POWER_SUPPLY_POWER_NOW=9112000,POWER_SUPPLY_ENERGY_FULL_DESIGN=57000000,POWER_SUPPLY_ENERGY_FULL=59230000,POWER_SUPPLY_ENERGY_NOW=44740000,POWER_SUPPLY_CAPACITY=75,POWER_SUPPLY_CAPACITY_LEVEL=Normal,POWER_SUPPLY_MODEL_NAME=02DL013,POWER_SUPPLY_MANUFACTURER=LGC,POWER_SUPPLY_SERIAL_NUMBER= 6349
1614153449,POWER_SUPPLY_NAME=BAT0,POWER_SUPPLY_TYPE=Battery,POWER_SUPPLY_STATUS=Discharging,POWER_SUPPLY_PRESENT=1,POWER_SUPPLY_TECHNOLOGY=Li-poly,POWER_SUPPLY_CYCLE_COUNT=582,POWER_SUPPLY_VOLTAGE_MIN_DESIGN=11580000,POWER_SUPPLY_VOLTAGE_NOW=11802000,POWER_SUPPLY_POWER_NOW=8568000,POWER_SUPPLY_ENERGY_FULL_DESIGN=57000000,POWER_SUPPLY_ENERGY_FULL=59230000,POWER_SUPPLY_ENERGY_NOW=44290000,POWER_SUPPLY_CAPACITY=74,POWER_SUPPLY_CAPACITY_LEVEL=Normal,POWER_SUPPLY_MODEL_NAME=02DL013,POWER_SUPPLY_MANUFACTURER=LGC,POWER_SUPPLY_SERIAL_NUMBER= 6349
-}

batt_stat_test_data4 :: T.Text
batt_stat_test_data4 = "1614153332,\n1614153449,\n"

batt_stat_test_data5 :: T.Text
batt_stat_test_data5 = "1614153332,POWER_SUPPLY_NAME=BAT0,\n1614153449,POWER_SUPPLY_NAME=BAT0,\n"

-- | for the sql PK ID/unix datetime value/1st field
bs_datetime :: Parsec Void T.Text Int
bs_datetime = digitsP

newtype Dt = Dt
  { dateTime :: Int
  } deriving (Eq, Show)

nullDt :: Dt
nullDt = Dt {dateTime = 0}

pPsTagName :: Parser Text
pPsTagName =
  choice
    [ string "POWER_SUPPLY_NAME"
    , string "POWER_SUPPLY_TYPE"
    , string "POWER_SUPPLY_STATUS"
    , string "POWER_SUPPLY_PRESENT"
    , string "POWER_SUPPLY_TECHNOLOGY"
    , string "POWER_SUPPLY_CYCLE_COUNT"
    , string "POWER_SUPPLY_VOLTAGE_MIN_DESIGN"
    , string "POWER_SUPPLY_VOLTAGE_NOW"
    , string "POWER_SUPPLY_POWER_NOW"
    , string "POWER_SUPPLY_ENERGY_FULL_DESIGN"
    , string "POWER_SUPPLY_ENERGY_FULL"
    , string "POWER_SUPPLY_ENERGY_NOW"
    , string "POWER_SUPPLY_CAPACITY"
    , string "POWER_SUPPLY_CAPACITY_LEVEL"
    , string "POWER_SUPPLY_MODEL_NAME"
    , string "POWER_SUPPLY_MANUFACTURER"
    , string "POWER_SUPPLY_SERIAL_NUMBER"
    ]

{-
pScheme :: Parser Text
pScheme = choice
  [ string "data"
  , string "file"
  , string "ftp"
  , string "http"
  , string "https"
  , string "irc"
  , string "mailto" ]

pUri :: Parser Uri
pUri = do
  r <- pScheme
  _ <- char ':'
  return (Uri r)

If we try to run pUri, we will see that it requires : to follow the scheme name now:

λ> parseTest pUri "irc"
-}
pDt :: Parser Dt
pDt = do
  r <- bs_datetime
  _ <- char ','
  return (Dt r)

-- | parser for the whole record
p_batt_stat_record :: Parser BattStatRecord
p_batt_stat_record = do
  dt <-
    try $ do
      dateTime <- dbg "D:Dt" L.decimal -- | pDt -- | unix date
      return Dt {..}
  _ <- char '|'
  string "POWER_SUPPLY_NAME" -- | _ <- pPsTagName
  _ <- char '='
  psName <- some alphaNumChar
  _ <- newline
  _ <- pPsTagName
  _ <- char '='
  psType <- T.pack <$> some alphaNumChar
  _ <- newline
  _ <- pPsTagName
  _ <- char '='
  psStatus <- some alphaNumChar
  _ <- newline
  _ <- pPsTagName
  _ <- char '='
  psPresent <- some alphaNumChar
  _ <- newline
  _ <- pPsTagName
  _ <- char '='
  psTechnology <- T.pack <$> some (alphaNumChar <|> char '-')
  _ <- newline
  _ <- pPsTagName
  _ <- char '='
  psCycleCount <- L.decimal
  _ <- newline
  _ <- pPsTagName
  _ <- char '='
  psVoltageMinDesign <- L.decimal
  _ <- newline
  _ <- pPsTagName
  _ <- char '='
  psVoltageNow <- L.decimal
  _ <- newline
  _ <- pPsTagName
  _ <- char '='
  psPowerNow <- some alphaNumChar
  _ <- newline
  _ <- pPsTagName
  _ <- char '='
  psEnergyFullDesign <- L.decimal
  _ <- newline
  _ <- pPsTagName
  _ <- char '='
  psEnergyFull <- L.decimal
  _ <- newline
  _ <- pPsTagName
  _ <- char '='
  psEnergyNow <- L.decimal
  _ <- newline
  _ <- pPsTagName
  _ <- char '='
  psCapacity <- L.decimal
  _ <- newline
  string "POWER_SUPPLY_CAPACITY_LEVEL" -- | _ <- pPsTagName
  --_ <- pPsTagName
  _ <- char '='
  psCapacityLevel <- T.pack <$> some alphaNumChar
  _ <- newline
  _ <- pPsTagName
  _ <- char '='
  psModelName <- T.pack <$> some alphaNumChar
  _ <- newline
  _ <- pPsTagName
  _ <- char '='
  psManufacturer <- T.pack <$> some alphaNumChar
  _ <- newline
  _ <- pPsTagName
  _ <- char '='
  psSerialNumber <- dbg "psSerialNumber" $ T.pack <$> some (alphaNumChar <|> spaceChar)
  return BattStatRecord {..}

p_batt_stat_record2 :: Parser BattStatRecord
p_batt_stat_record2 = do
  dt <-
    try $ do
      dateTime <- dbg "D:Dt" L.decimal -- | pDt -- | unix date
      return Dt {..}
  _ <- char ','
  _ <- pPsTagName
  _ <- char '='
  psName <- dbg "D:psName" $ some alphaNumChar
  _ <- char ','
  _ <- pPsTagName
  _ <- char '='
  psType <- dbg "D:psType" $ T.pack <$> some alphaNumChar
  _ <- char ','
  _ <- pPsTagName
  _ <- char '='
  psStatus <- dbg "D:psStatus" $ some alphaNumChar
  _ <- char ','
  _ <- pPsTagName
  _ <- char '='
  psPresent <- dbg "D:psPresent" $ some alphaNumChar
  _ <- char ','
  _ <- pPsTagName
  _ <- char '='
  psTechnology <- dbg "D:psTechnology" $ T.pack <$> some (alphaNumChar <|> char '-')
  _ <- char ','
  _ <- pPsTagName
  _ <- char '='
  psCycleCount <- dbg "D:psCycleCount" L.decimal
  _ <- char ','
  _ <- pPsTagName
  _ <- char '='
  psVoltageMinDesign <- dbg "D:psVoltageMinDesign" L.decimal
  _ <- char ','
  _ <- pPsTagName
  _ <- char '='
  psVoltageNow <- dbg "D:psVoltageNow" L.decimal
  _ <- char ','
  _ <- pPsTagName
  _ <- char '='
  psPowerNow <- dbg "D:psPowerNow" $ some alphaNumChar
  _ <- char ','
  _ <- pPsTagName
  _ <- char '='
  psEnergyFullDesign <- dbg "D:psEnergyFullDesign" L.decimal
  _ <- char ','
  _ <- pPsTagName
  _ <- char '='
  psEnergyFull <- dbg "D:psEnergyFull" L.decimal
  _ <- char ','
  _ <- pPsTagName
  _ <- char '='
  psEnergyNow <- dbg "D:psEnergyNow" L.decimal
  _ <- char ','
  _ <- pPsTagName
  _ <- char '='
  psCapacity <- dbg "D:psCapacity" L.decimal
  _ <- char ','
  string "POWER_SUPPLY_CAPACITY_LEVEL" -- | _ <- pPsTagName
  --_ <- pPsTagName
  _ <- char '='
  psCapacityLevel <- dbg "D:psCapacityLevel" $ T.pack <$> some alphaNumChar
  _ <- char ','
  _ <- pPsTagName
  _ <- char '='
  psModelName <- dbg "D:psModelName" $ T.pack <$> some alphaNumChar
  _ <- char ','
  _ <- pPsTagName
  _ <- char '='
  psManufacturer <- dbg "D:psManufacturer" $ T.pack <$> some alphaNumChar
  _ <- char ','
  _ <- pPsTagName
  _ <- char '='
  psSerialNumber <- dbg "psSerialNumber" $ T.pack <$> some (digitChar <|> spaceChar)
  _ <- newline
  --newline >> return () <|> eof
  return BattStatRecord {..}

p_batt_stat_record3 :: Parser BattStatRecord
p_batt_stat_record3 = do
  dt <-
    try $ do
      dateTime <- L.decimal <?> "D:Dt" 
      return Dt {..}
  _ <- char ','
  _ <- pPsTagName
  _ <- char '='
  psName <- some alphaNumChar <?> "D:psName"
  _ <- char ','
  _ <- newline
  --void eol
  --newline >> return () <|> eof
  return BattStatRecord {..}
  -- <?> "p_batt_stat_record3"

--eolof :: TextParser m ()
--eolof :: Parser m ()
--eolof = (newline >> return ()) <|> eof

-- | Whole record
type BattStatRecords = [BattStatRecord]

data BattStatRecord =
  BattStatRecord
    { dt :: Dt
    , psName :: String
    , psType :: Text
    , psStatus :: String
    , psPresent :: String
    , psTechnology :: Text
    , psCycleCount :: Int
    , psVoltageMinDesign :: Int
    , psVoltageNow :: Int
    , psPowerNow :: String
    , psEnergyFullDesign :: Int
    , psEnergyFull :: Int
    , psEnergyNow :: Int
    , psCapacity :: Int
    , psCapacityLevel :: Text
    , psModelName :: Text
    , psManufacturer :: Text
    , psSerialNumber :: Text
    }
  deriving (Eq, Show)

nullBattStatRecord :: BattStatRecord
nullBattStatRecord =
  BattStatRecord
    { dt = nullDt
    , psName = ""
    , psType = ""
    , psStatus = ""
    , psPresent = ""
    , psTechnology = ""
    , psCycleCount = 0
    , psVoltageMinDesign = 0
    , psVoltageNow = 0
    , psPowerNow = ""
    , psEnergyFullDesign = 0
    , psEnergyFull = 0
    , psEnergyNow = 0
    , psCapacity = 0
    , psCapacityLevel = ""
    , psModelName = ""
    , psManufacturer = ""
    , psSerialNumber = ""
    }

{-
-- | PsType
data PsType
  = PsName
  | PsType
  | PsStatus
  | PsPresent
  | PsTechnology
  | PsCycleCount
  | PsVoltageMinDesign
  | PsVoltageNow
  | PsPowerNow
  | PsEnergyFullDesign
  | PsEnergyFull
  | PsEnergyNow
  | PsCapacity
  | PsCapacityLevel
  | PsModelName
  | PsManufacturer
  | PsSerialNumber
  deriving (Eq, Show)

pType :: Parser PsType
pType =
  choice
    [ PsName <$ string "POWER_SUPPLY_NAME"
    , PsType <$ string "POWER_SUPPLY_TYPE"
    , PsStatus <$ string "POWER_SUPPLY_STATUS"
    , PsPresent <$ string "POWER_SUPPLY_PRESENT"
    , PsTechnology <$ string "POWER_SUPPLY_TECHNOLOGY"
    , PsCycleCount <$ string "POWER_SUPPLY_CYCLE_COUNT"
    , PsVoltageMinDesign <$ string "POWER_SUPPLY_VOLTAGE_MIN_DESIGN"
    , PsVoltageNow <$ string "POWER_SUPPLY_VOLTAGE_NOW"
    , PsPowerNow <$ string "POWER_SUPPLY_POWER_NOW"
    , PsEnergyFullDesign <$ string "POWER_SUPPLY_ENERGY_FULL_DESIGN"
    , PsEnergyFull <$ string "POWER_SUPPLY_ENERGY_FULL"
    , PsEnergyNow <$ string "POWER_SUPPLY_ENERGY_NOW"
    , PsCapacity <$ string "POWER_SUPPLY_CAPACITY"
    , PsCapacityLevel <$ string "POWER_SUPPLY_CAPACITY_LEVEL"
    , PsModelName <$ string "POWER_SUPPLY_MODEL_NAME"
    , PsManufacturer <$ string "POWER_SUPPLY_MANUFACTURER"
    , PsSerialNumber <$ string "POWER_SUPPLY_SERIAL_NUMBER"
    ]

-- | POWER_SUPPLY_NAME=BAT0
data PsRecord  = PsRecord
  { psType :: PsType
  } deriving (Eq, Show)
-}
-- | CSV record
data CsvRecord =
  CsvRecord
    { csvDt :: Text
    , csvPsStatus :: Text
    , csvPsPresent :: Text
    , csvPsTechnology :: Text
    , csvPsCycleCount :: Int
    , csvPsVoltageMinDesign :: Int
    , csvPsVoltageNow :: Int
    , csvPsPowerNow :: Text
    , csvPsEnergyFullDesign :: Int
    , csvPsEnergyFull :: Int
    , csvPsEnergyNow :: Int
    , csvPsCapacity :: Int
    , csvPsCapacityLevel :: Text
    }
  deriving (Eq, Show)

type CsvRecords = [CsvRecord]

--convert_to_csv :: BattStatRecord -> IO Text
--convert_to_csv :: (Monad m) => BattStatRecord -> m b
convert_to_csv BattStatRecord { dt
                              , psName
                              , psStatus
                              , psPresent
                              , psCycleCount
                              , psVoltageNow
                              , psPowerNow
                              , psEnergyFull
                              , psEnergyNow
                              , psCapacity
                              } = do
  print_date <- convert_unix_date_to_txt (dateTime dt)
  return $ T.pack (print_date ++ "," ++ psName ++ "," ++ psStatus ++ "," ++ psPresent ++ "," ++ show psCycleCount ++ "," ++ show psVoltageNow ++ "," ++ psPowerNow ++ "," ++ show psEnergyFull ++ "," ++ show psEnergyNow ++ "," ++ show psCapacity)
  where
    --convert_unix_date_to_txt :: Int -> IO Text
    convert_unix_date_to_txt unix_date = do
      let dateCmd = date_cmd unix_date
      date_str <- readProcess "date" dateCmd ""
      --return (T.init $ T.pack date_str)
      return $ init date_str

--comma :: Text
comma :: [Char]
--comma = T.pack ","
comma = ","

-- | From here, relevant to batt-stat
-- | format the params for the `date` cmd as a list
--date_cmd dt_num = [T.pack ("--date=@" ++ show dt_num), T.pack "+%FT%T"]
date_cmd :: Int -> [[Char]]
date_cmd dt_num = ["--date=@" ++ show dt_num, "+%FT%T"]

{- | readProcess \"date\" [\"--date=@1609862298\", \"+%FT%T\"] \"\""
-- (_, Just hout, _, _) <- createProcess (proc \"date\" [\"--date=@1609862298\"])"
--convert_unix_date_to_txt :: (Monad a) => Int -> m a
--convert_unix_date_to_txt :: Int -> IO ()
convert_unix_date_to_txt unix_date = do
  let dateCmd = date_cmd unix_date
  date_str <- readProcess "date" dateCmd ""
  return (init date_str)
-}
-- | Record:
newtype VersionNumberName =
  VersionNumberName
    { versionVarName :: T.Text
    }
  deriving (Eq)

instance Show VersionNumberName where
  show VersionNumberName {..} =
    printf "VersionNumberName {versionVarName = \"%s\"}\n" versionVarName

-- | null record
nullVersionNumberName :: VersionNumberName
nullVersionNumberName = VersionNumberName ""

-- | and its Parser
p_ver_var_name :: Parser VersionDetails
p_ver_var_name = do
  _ <- defineP -- | define
  _ <- char '('
  _ <- char '['
  verNumName <-
    optional . try $ do
      versionVarName <- dbg "ver_var_name" verVarNameP -- | VERSION_NUMBER
      return VersionNumberName {..}
  _ <- char ']'
  _ <- printChar -- | ','
  _ <- char '['
  verNum <-
    try $
    -- | WORKS for digitChar
    --majorV <- dbg "majorV" $ some digitChar   -- | 0..9
     do
      majorV <- dbg "majorV" L.decimal -- | 0..9
      _ <- char '.'
    -- | WORKS for digitChar
    -- minorV <- dbg "minorV" $ some digitChar   -- | 0..9
      minorV <- dbg "minorV" L.decimal -- | 0..9
      _ <- char '.'
    -- | WORKS for digitChar
    -- patchV <- dbg "patchV" $ some digitChar   -- | 0..9
      patchV <- dbg "patchV" L.decimal -- | 0..9
      return VersionNumber {..}
  char ']' -- | non funziona: void (char ']')
  char ')' -- | non funziona: void (char ')')
  _ <- dnlP -- | dnl (m4 stop processing)
  return VersionDetails {..}

-- | data type for the version number
data VersionNumber =
  VersionNumber
    { majorV :: Int
    , minorV :: Int
    , patchV :: Int
    }
  deriving (Eq, Show)

nullVersionNumber :: VersionNumber
nullVersionNumber = VersionNumber 999 999 999

-- | data type for the version number var name (as known by M4)
-- and the version number all wrapped in a M4 define statement
data VersionDetails =
  VersionDetails
    { verNumName :: Maybe VersionNumberName
    , verNum :: VersionNumber
    }
  deriving (Eq, Show)

nullVersionDetails :: VersionDetails
nullVersionDetails = VersionDetails Nothing nullVersionNumber

-- |
verVarNameP :: Parser Text
verVarNameP = string' "VERSION_NUMBER"

-- | This seems to work also with string (instead of string') and with T.pack
defineP :: Parser Text
defineP = string' $ T.pack "define"

dnlP :: Parser Text
dnlP = string' "dnl"

-- | function to build a list from [majorV, minorV, patchV]
buildVersion :: VersionDetails -> Version
buildVersion verDets =
  makeVersion
    [ (majorV . verNum) verDets
    , (minorV . verNum) verDets
    , (patchV . verNum) verDets
    ]

-- | From here down relevant to owwica
-- The marker for the title of a man page
newtype ManTitle =
  ManTitle
    { title :: T.Text
    }
  deriving (Eq, Show)

p_man_title :: Parser ManTitle
p_man_title = do
  _ <- dbg "marker for the title" man_name_mark
  char '\n' -- | non funziona: void (newline)
  title <- dbg "actual title value" (T.pack <$> some printChar)
  return (ManTitle title)

man_name_mark :: Parser Text
man_name_mark = string $ T.pack ".SH NAME"

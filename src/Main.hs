{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}

module Main where

import Control.Monad
import Data.Either
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.IO
import qualified Data.Text.IO as TIO
import Data.Version
import Internal
import Prelude as P
import System.Environment (getArgs, getProgName)
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Printf (printf)
--import qualified Data.ByteString.Char8 as C8

import Control.Applicative hiding (many, some)

{-
-}
main :: IO ()
main = do
  -- | WORKS: b/c, in this mode, the parser stops at the 1st match.
  -- | also if its 'T.pack'd
  P.putStrLn "starting ... " 
  {-
  P.putStr "WORKS: hits 'a' and stops: " 
  parseTest (satisfy (== 'a') :: Parser Char) "a"
  -- | FAILS: because '<* eof' says I am going to continue until eof (end of
  -- file)
  P.putStr "FAILS: continues until eof: "
  parseTest (satisfy (== 'a') <* eof :: Parser Char) "aVERSION_NUMBER"
  P.putStr "WORKS: 'define' is the 1st hit: " 
  parseTest (string' "define" :: Parser Text) "define([VERSION_NUMBER],[0.0.1])dnl"
  P.putStr "WORKS: using a function returning a Parser: " 
  parseTest defineP "define([VERSION_NUMBER],[0.0.1])dnl"
  parseTest (newline :: Parser Char) "\n"
  P.putStr "FAILS: VERSION_NUMBER is not the 1st thing detected: "
  parseTest (string' "VERSION_NUMBER" <* eof :: Parser Text)  "define([VERSION_NUMBER],[0.0.1])dnl"
  -- | bash_man_file = ".SH NAME\nbash \\- GNU Bourne-Again SHell"
  P.putStrLn "WORKS: it needs the whole line: "
  parseTest p_ver_var_name "define([VERSION_NUMBER],[0.0.1])dnl"
  P.putStr "WORKS: "
  parseTest (string' ".SH NAME\n" :: Parser Text) bash_man_file
  P.putStr "?????: "
  parseTest (string' ".SH NAME\n" :: Parser Text) bash_man_file
  P.putStr "WORKS: using a function returning a Parser: " 
  parseTest p_man_title bash_man_file
  -- | bash_man_file = ".SH NAME\nbash \\- GNU Bourne-Again SHell"
  -}
  {-
  let r2_t1 = Rec2{age = 61, email = "agander@gmail.com"}
  let r1_t1 = Rec1 "giles" r2_t1
  P.putStrLn "r1_t1:"
  print r1_t1
  print $ (email . details) r1_t1
  print $ details r1_t1
  -}

  -- | getContents from piped data from sqlite SELECT statement
  --lines <- TIO.getContents
  lines <- TIO.readFile "data/batt-stat_5recs_conv_to_commas.txt"
  --parseTest (many(p_batt_stat_record2 <* (newline >> return ()) <|> eof)) batt_stat_test_data3
  --parseTest (many p_batt_stat_record2) batt_stat_test_data3

  {- | batt-stat parsing:

  P.putStrLn
    "WORKS: get the (unix) date from: parseTest pDt batt_stat_test_data1:"
  parseTest pDt batt_stat_test_data1
  --P.putStrLn "WORKS: get the (unix) date from: parseTest p_batt_stat_record batt_stat_test_data:"
  --parseTest p_batt_stat_record batt_stat_test_data

  let bsr1 = fromRight nullBattStatRecord $
             runParser p_batt_stat_record "" batt_stat_test_data1
  
  P.putStrLn "D:Start parse of batt_stat_test_data2"
  let bsr2 = fromRight nullBattStatRecord $
             runParser p_batt_stat_record "" batt_stat_test_data2
  
  P.putStrLn "D:Start parse of batt_stat_test_data3"
  let bsr3 = fromRight nullBattStatRecord $
             runParser (p_batt_stat_record2 <* eof) "" batt_stat_test_data3

  P.putStrLn "D:Start parse of batt_stat_test_data4"
  let bsr4 = fromRight nullBattStatRecord $
             runParser (p_batt_stat_record2 <* eof) "" batt_stat_test_data4

  -}
  P.putStrLn "D:Start parse of batt_stat_test_data5"
  let bsrs5_ex_parse = runParser (many p_batt_stat_record3 <* eof) "" batt_stat_test_data5
      bsr5s = map (fromRight nullBattStatRecord) bsrs5_ex_parse

  {-
  P.putStrLn "D:Start parse of data/batt-stat_5recs_conv_to_commas.txt"
  let bsr_lines = fromRight nullBattStatRecord $
             mapM_ (runParser (many p_batt_stat_record2)) "" lines
  -}

  --P.putStrLn "convert_to_csv [bsr1,bsr2]:"
  --convert_to_csv bsr1
  --strs <- mapM convert_to_csv [bsr1, bsr2]
  --strs <- mapM convert_to_csv [bsr_lines]
  --strs <- mapM convert_to_csv [bsr3]
  --strs <- mapM convert_to_csv [bsr4]
  --mapM_ print strs 
  --mapM convert_to_csv [bsr_lines] >>= mapM_ print 
  --mapM convert_to_csv [bsr4] >>= mapM_ print 
  mapM convert_to_csv bsr5s >>= mapM_ print 

  --fmap convert_to_csv bsr4 >>= mapM_ print 

  -- | recurse.com test
  -- Write a program that prints out the numbers 1 to 100 (inclusive).
  -- If the number is divisible by 3, print Crackle instead of the number.
  -- If it's divisible by 5, print Pop.
  -- If it's divisible by both 3 and 5, print CracklePop.
  -- mapM cracklePop [1..100]

  {-
  let 
    res = runParser p_ver_var_name "" "define([],[0.0.1])dnl"
  --P.putStrLn "?"
  --P.putStrLn $ fromRight (VersionDetails {..}) res
  let ver = buildVersion $
            fromRight nullVersionDetails $
            runParser p_ver_var_name "" "define([],[0.0.1])dnl"
  P.putStrLn "WORKS: print of a constructed Version:" 
  print $ showVersion ver
  -- |Show WIP of VersionNumberName
  let ver1 = VersionNumberName "VERSION_NUMBER"
  printf "VersionNumberName {\"%s\"}\n" $ versionVarName ver1
  -}
  P.putStrLn "... fin"

{-
cracklePop :: Int -> IO ()
cracklePop num = 
  if num `mod` 3 == 0 && num `mod` 5 == 0 then P.putStr "CracklePop, "
    else if num `mod` 3 == 0 then P.putStr "Crackle, "
      else if num `mod` 5 == 0 then P.putStr "Pop, "
        else P.putStr $ show num ++ ", "
-}

update_name :: [Char] -> Rec1 -> Rec1
-- | WORKS:
--update_name str r1@Rec1 {name=n,details=r2@Rec2{age=a,email=e}} = Rec1{name=str,details=r2}
-- | WORKS:
--update_name str r1@Rec1 {name=n,details=r2@Rec2{}} = Rec1{name=str,details=r2}
-- | WORKS:
update_name str r1@Rec1 {name=n,details=r2@Rec2{}} = r1{name=str,details=r2}

update_age :: Int -> Rec2 -> Rec2
update_age n r2@Rec2 {age = a, email = email} = Rec2 {age = a + n, email}

data Rec1 = Rec1
  { name :: String
  , details :: Rec2
  } deriving (Eq, Show)

data Rec2 = Rec2
  { age :: Int
  , email :: T.Text -- String
  } deriving (Eq, Ord)

--instance Show Rec1 where
  --show Rec1 {..} = printf "Rec1 {name = %s, details = \"%s\"}\n" name details

instance Show Rec2 where
  show Rec2 {..} = printf "Rec2 {age = %d, email = \"%s\"}\n" age email

